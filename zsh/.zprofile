Open directory in terminal
px() {
  cd "$HOME/$1"
}

# Bumps a tag version
bump() {
  git commit -m "Bump to $1" && git tag -a $1 -m "Bump version $1" && git push && git push --tags;
}

# Downloads specified version of theme from current repo
function dlv() {
  open "https://bitbucket.org/`git remote -v | grep bitbucket | head -n 1 | sed -e 's/.*:\(.*\)\/\(.*\)\.git.*/\1\/\2/'`/downloads/$1-v$2.zip"
}

# Clones specific repo
function gitClone() {
  git clone https://bitbucket.org/$1/$2.git
}

# Opens current repo in BB
function bb() {
  open "https://bitbucket.org/`git remote -v | grep bitbucket | head -n 1 | sed -e 's/.*:\(.*\)\/\(.*\)\.git.*/\1\/\2/'`"
}

# Opens current branch as a PR
function bbpr() {
  open "https://bitbucket.org/`git remote -v | grep bitbucket | head -n 1 | sed -e 's/.*:\(.*\)\/\(.*\)\.git.*/\1\/\2/'`/pull-request/new"
}